import json
import random
import string
import time


import db
import settings
from tornado import template


class BasePoint(object):
    html_template = 'templates/point.html'

    def __init__(self, x, y, value=settings.EMPTY_VALUE, div_postfix=None):
        self.x = x
        self.y = y
        self.value = value
        self.div_postfix = div_postfix

    def __str__(self):
        return str(self.value)

    def __repr__(self):
        return '({x}, {y}, {value})'.format(x=self.x, y=self.y, value=self.value)

    def as_html(self):
        if self.value:
            loader = template.Loader(settings.TEMPLATE_PATH)
            t = loader.load(self.html_template)
            return t.generate(point=self)
        else:
            return ''

    def as_dict(self):
        return {'x': self.x, 'y': self.y, 'value': self.value, 'div_id': self.div_id, 'td_id': self.td_id,
                'html': self.as_html()}

    @property
    def div_id(self):
        source_value = self.value if self.value else ''
        if not self.div_postfix:
            random_part = ''.join([str(random.choice(string.digits)) for _ in range(5)])
            div_postfix = str(int(time.time())) + random_part
        else:
            div_postfix = self.div_postfix
        return settings.SOURCE_VALUE_SPLITTER.join(map(str, [source_value, div_postfix]))

    @property
    def td_id(self):
        return 'td_{x}_{y}'.format(x=self.x, y=self.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y


class ViewMixin(object):
    history_template = 'templates/history_item.html'
    client_key_splitter = '_'

    def check_id(self, obj_id):
        return unicode(obj_id).isdigit()

    def check_client_key(self, client_key):
        try:
            random_key, timestamp_key = client_key.split(self.client_key_splitter)
        except ValueError:
            return False
        else:
            return random_key.isalnum() and timestamp_key.isdigit()

    def write_history(self, client_id, play_field, start_point, end_point, scored_points=0):
        play_field = json.dumps([[point.as_dict() for point in line] for line in play_field])
        start_point = json.dumps(start_point.as_dict()) if start_point else ''
        end_point = json.dumps(end_point.as_dict()) if end_point else ''
        return db.write_history_item(client_id, play_field, start_point, end_point, scored_points)

    def get_history_item_context(self, history_id):
        return self._get_history_item_context(db.get_history_item_by_id(history_id))

    def _get_history_item_context(self, row):
        history_id, author_id, play_field, start_point, end_point, scored_points = row
        play_field = json.loads(play_field)
        start_point = json.loads(start_point) if start_point else ''
        end_point = json.loads(end_point) if end_point else ''
        history_url = '{base_url}?history_id={history_id}'.\
            format(base_url=self.reverse_url('history'), history_id=history_id)
        return {'author_id': author_id, 'play_field': play_field, 'start_point': start_point, 'end_point': end_point,
                'scored_points': scored_points, 'history_id': history_id, 'history_url': history_url}

    def get_rendered_history_item(self, history_id):
        history_item = self.get_history_item_context(history_id)
        return self._get_rendered_history_item(history_item)

    def _get_rendered_history_item(self, history_item):
        loader = template.Loader(settings.TEMPLATE_PATH)
        t = loader.load(self.history_template)
        return history_item['history_id'], t.generate(**history_item)

    def get_rendered_history_row(self, row):
        context = self._get_history_item_context(row)
        loader = template.Loader(settings.TEMPLATE_PATH)
        t = loader.load(self.history_template)
        return t.generate(**context)
