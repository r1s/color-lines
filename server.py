import copy
from itertools import chain
import random
import string
import time
import json
from tornado import websocket, web, ioloop

import db
import settings
from utils.check_lines import check_lines
from utils.check_path import check_path
from utils.points import convert_source_to_matrix, get_next_points
from base import BasePoint, ViewMixin

cl = []


class SocketHandler(websocket.WebSocketHandler):

    def open(self):
        if self not in cl:
            cl.append(self)

    def on_close(self):
        if self in cl:
            client_key = self.get_secure_cookie(settings.CLIENT_ID_COOKIE_NAME)
            client_id = db.get_client_by_key(client_key)
            db.clean_history(client_id)
            cl.remove(self)


class IndexHandler(web.RequestHandler, ViewMixin):
    template = 'templates/index.html'
    title = 'Color lines'
    page_title = 'Color lines'

    play_field_width = settings.PLAY_FIELD_WIDTH
    play_field_height = settings.PLAY_FIELD_HEIGHT

    def get(self):
        client_key = self.gen_client_key()
        self.set_secure_cookie(settings.CLIENT_ID_COOKIE_NAME, client_key)
        client_id = db.get_client_by_key(client_key)
        self.render(self.template, **self.get_context(client_id))

    def gen_client_key(self, size=16):
        chars = string.ascii_letters + string.digits

        get_key = lambda: '{random_key}{client_key_splitter}{timestamp_key}'.\
            format(random_key=''.join(random.choice(chars) for _ in range(size)), timestamp_key=str(int(time.time())),
                   client_key_splitter=self.client_key_splitter)

        full_key = get_key()
        while db.get_client_by_key(full_key):
            full_key = get_key()
        db.write_new_client(full_key)
        return full_key

    def get_play_field(self):
        return convert_source_to_matrix()

    def get_context(self, client_id):
        play_field = self.get_play_field()
        next_points = get_next_points(play_field)

        for next_point in next_points:
            play_field[next_point.y][next_point.x] = next_point

        history_id = self.write_history(client_id, play_field, None, None, 0)

        return {'title': self.title, 'page_title': self.page_title, 'play_field': play_field,
                'settings': settings, 'history_item': self.get_rendered_history_item(history_id),
                'ws_url': self.reverse_url('ws'), 'api_url': self.reverse_url('api')}


class ApiHandler(web.RequestHandler, ViewMixin):

    def get_context(self, path_valid, start_point, end_point, points_add, points_delete, scored_points, history_id):
        points_add = [point.as_dict() for point in points_add]
        points_delete = [point.as_dict() for point in points_delete]
        history_item = [self.get_rendered_history_item(history_id)] if history_id else []
        start_point = start_point.as_dict() if start_point else ''
        end_point = end_point.as_dict() if end_point else ''
        return {'points_add': points_add, 'points_delete': points_delete, 'path_valid': path_valid,
                'start_point': start_point, 'end_point': end_point, 'scored_points': scored_points,
                'history_list': history_item, 'restore_history': False}

    def post(self, *args, **kwargs):
        client_key = self.get_secure_cookie(settings.CLIENT_ID_COOKIE_NAME)

        if not self.check_client_key(client_key):
            raise web.HTTPError(400)

        client_id = db.get_client_by_key(client_key)

        points_add = []
        points_delete = []
        scored_points = db.get_last_scored_points(client_id) or 0
        history_id = None

        points = json.loads(self.get_argument('data'))
        current_point_data = json.loads(self.get_argument('current_point'))

        play_field = convert_source_to_matrix(points)
        history_play_field = copy.deepcopy(play_field)

        end_point = play_field[current_point_data['target_row']][current_point_data['target_cell']]
        start_point = BasePoint(current_point_data['source_cell'], current_point_data['source_row'], end_point.value)

        path_valid = check_path(play_field, start_point, end_point)
        if path_valid:
            points_delete, current_scored_points = check_lines(play_field)
            scored_points += current_scored_points
            if not points_delete:
                points_add = get_next_points(play_field)
            else:
                for point_delete in points_delete:
                    history_play_field[point_delete.y][point_delete.x] = BasePoint(point_delete.x, point_delete.y, None)
            history_id = self.write_history(client_id, history_play_field, start_point, end_point, scored_points)
        elif start_point == end_point:
            start_point = None
            end_point = None

        context = self.get_context(path_valid, start_point, end_point, points_add, points_delete, scored_points,
                                   history_id)

        data = json.dumps(context)

        for c in cl:
            c_cookie = c.get_secure_cookie(settings.CLIENT_ID_COOKIE_NAME)
            if c_cookie == client_key:
                c.write_message(data)


class HistoryHandler(web.RequestHandler, ViewMixin):

    def get_context(self, history_id):
        history = db.get_history_before(history_id)
        history_list = [self._get_history_item_context(history_row) for history_row in history]
        current_item = self._get_history_item_context(history[-1])
        history_list = [self._get_rendered_history_item(history_item) for history_item in history_list]

        points_add = list(chain(*current_item['play_field']))
        start_point = current_item['start_point']
        end_point = current_item['end_point']
        scored_points = current_item['scored_points']
        points_delete = []

        return {'points_add': points_add, 'points_delete': points_delete, 'path_valid': False,
                'start_point': start_point, 'end_point': end_point, 'scored_points': scored_points,
                'history_list': history_list, 'restore_history': True}

    def post(self, *args, **kwargs):
        client_key = self.get_secure_cookie(settings.CLIENT_ID_COOKIE_NAME)

        if not self.check_client_key(client_key):
            raise web.HTTPError(400)

        client_id = db.get_client_by_key(client_key)
        history_id = self.get_argument('history_id')

        if not self.check_id(history_id):
            raise web.HTTPError(400)

        history_row = db.get_history_item_by_id(history_id)
        if history_row and history_row[1] == client_id:
            data = self.get_context(history_id)
            db.clean_history_after(client_id, history_id)
            for c in cl:
                c_cookie = c.get_secure_cookie(settings.CLIENT_ID_COOKIE_NAME)
                if c_cookie == client_key:
                    c.write_message(data)


app = web.Application([web.URLSpec(r'/', IndexHandler, name='index'),
                       web.URLSpec(r'/ws/', SocketHandler, name='ws'),
                       web.URLSpec(r'/api/', ApiHandler, name='api'),
                       web.URLSpec(r'/history/', HistoryHandler, name='history'),
                       (r'/static/(.*)', web.StaticFileHandler, {'path': 'static/'}),],
                      cookie_secret=settings.COOKIE_SECRET)

if __name__ == '__main__':
    db.create_tables()
    app.listen(settings.PORT, settings.HOST)
    print('Open http://{host}:{port}'.format(host=settings.HOST, port=settings.PORT))
    ioloop.IOLoop.instance().start()