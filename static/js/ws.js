var ws = window.ws;
var	rd = REDIPS.drag;
var $message = $('#message');
var $scored_points = $('#scored_points');
var postdata = function(event){
      event.preventDefault();
      $.post($(this).attr('href'), $(this).serialize());
};

ws.onopen = function(){
  $message.attr("class", 'label label-success');
  $message.text('open');
};
ws.onmessage = function(ev){
  var table_el = document.getElementById('table');
  rd.enableTable(false, table_el);
  var json = JSON.parse(ev.data);

  if (json.path_valid == true || json.restore_history == true) {
    $.each(json.points_add, function (key, point_data) {
      var td_el = document.getElementById(point_data.td_id);

      if (td_el !== null) {
        $.each(td_el.childNodes, function (key, tr_child) {
        if (tr_child != undefined && tr_child != null) {
          td_el.removeChild(tr_child);}
        });
        td_el.innerHTML = '';

        if ((json.restore_history == true && point_data.value != null) || (json.path_valid == true)) {
          var newDiv = document.createElement('div');
          newDiv.id = point_data.div_id;
          newDiv.className = "drag";
          newDiv.style.cursor = "move";
          newDiv.innerHTML = point_data.html;
          td_el.appendChild(newDiv);
          var div_el = document.getElementById(point_data.div_id);
          rd.enableDrag(true, div_el);
        }


      }
    });

    $.each(json.points_delete, function (key, point_data) {
      var div_el = document.getElementById(point_data.div_id);
      if (div_el !== null) {
        rd.deleteObject(div_el);
      }
    });

    $scored_points.text(json.scored_points);

    var hist_table = document.getElementById('history_table');

    if (json.restore_history == true) {
      for(var i = hist_table.rows.length - 1; i > 0; i--)
      {hist_table.deleteRow(i);}
    }

    $.each(json.history_list, function (key, history_item) {
      var row_count = hist_table.rows.length;
      var row = hist_table.insertRow(row_count);
      row.innerHTML = history_item[1];
      var link_id = 'a#link_post_' + history_item[0].toString();
      $(link_id).click(postdata);
      });
  }
  else
  {
    if (json.start_point != '' &&  json.end_point != '') {
      var td_start_point = document.getElementById(json.start_point.td_id);
      var newDiv = document.createElement('div');
      newDiv.id = json.start_point.div_id;
      newDiv.className = "drag";
      newDiv.style.cursor = "move";
      newDiv.innerHTML = json.start_point.html;
      td_start_point.appendChild(newDiv);
      var div_el = document.getElementById(json.start_point.div_id);
      rd.enableDrag(true, div_el);
      var td_end_point = document.getElementById(json.end_point.td_id);
      $.each(td_end_point.childNodes, function (key, tr_child) {
        if (tr_child != undefined && tr_child != null) {
          td_end_point.removeChild(tr_child);}
        });
      td_end_point.innerHTML = '';
    }
  }

rd.enableTable(true, table_el);
};
ws.onclose = function(ev){
  $message.attr("class", 'label label-important');
  $message.text('closed');
};
ws.onerror = function(ev){
  $message.attr("class", 'label label-warning');
  $message.text('error occurred');
};
$('a[id^="link_post_"]').click(postdata);