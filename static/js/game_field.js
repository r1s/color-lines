/*jslint white: true, browser: true, undef: true, nomen: true, eqeqeq: true, plusplus: false, bitwise: true, regexp: true, strict: true, newcap: true, immed: true, maxerr: 14 */
/*global window: false, REDIPS: true */

/* enable strict mode */
"use strict";

// create redips container
var redips = {};

// redips initialization
redips.init = function () {
	// reference to the REDIPS.drag object
	var	rd = REDIPS.drag;
	// define border style (this should go before init() method)
	rd.style.borderEnabled = 'none';
	// initialization
	rd.init();
	// set hover color
	rd.hover.colorTd = '#FFE885';
	// DIV elements can be dropped to the empty cells only
	rd.dropMode = 'single';

    rd.event.finish = function () {

        var pos = rd.getPosition();
        var pos_json = {};
        if (pos != -1)
        {
            pos_json['target_table'] = pos[0];
            pos_json['target_row'] = pos[1];
            pos_json['target_cell'] = pos[2];
            pos_json['source_table'] = pos[3];
            pos_json['source_row'] = pos[4];
            pos_json['source_cell'] = pos[5];
        }
        //console.log(pos_json);
        $.post( window.api_url, { data: rd.saveContent('table', 'json'), current_point: JSON.stringify(pos_json)} );
    };
};

// add onload event listener
if (window.addEventListener) {
	window.addEventListener('load', redips.init, false);
}
else if (window.attachEvent) {
	window.attachEvent('onload', redips.init);
}