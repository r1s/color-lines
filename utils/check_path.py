import heapq
import copy
import settings
from base import BasePoint


class Matrix(list):
    def __init__(self, matrix):
        super(Matrix, self).__init__(matrix)
        self.len_x = len(matrix[0])
        self.len_y = len(matrix)

    def get_around_points(self, point, closed_point_list):
        """
        Method for get points around self point. For A* implementation
        :param point:
        :param closed_point_list:
        :return list of points:
        """
        points = []
        if point.x != 0:
            x = point.x - 1
            tmp_point = Point(x, point.y, self[point.y][x].value, point.ceil_point, point)
            if tmp_point.non_barier() and not tmp_point.was_passed(closed_point_list):
                points.append(tmp_point)
        if point.y != 0:
            y = point.y - 1
            tmp_point = Point(point.x, y, self[y][point.x].value, point.ceil_point, point)
            if tmp_point.non_barier() and not tmp_point.was_passed(closed_point_list):
                points.append(tmp_point)

        if point.y != self.len_y - 1:
            y = point.y + 1
            tmp_point = Point(point.x, y, self[y][point.x].value, point.ceil_point, point)
            if tmp_point.non_barier() and not tmp_point.was_passed(closed_point_list):
                points.append(tmp_point)
        if point.x != self.len_x - 1:
            x = point.x + 1
            tmp_point = Point(x, point.y, self[point.y][x].value, point.ceil_point, point)
            if tmp_point.non_barier() and not tmp_point.was_passed(closed_point_list):
                points.append(tmp_point)

        return points


class Point(BasePoint):
    def __init__(self, coord_x, coord_y, value, ceil_point=None, parent=None):
        super(Point, self).__init__(coord_x, coord_y, value)
        self.ceil_point = ceil_point
        self.parent = parent
        self.base_weight = 10
        self.g = self.parent.g if self.parent else 10

    def get_h(self):
        return (abs(self.ceil_point.x - self.x) + abs(self.ceil_point.y - self.y)) * 10 if self.ceil_point else None

    def get_f(self):
        h = self.get_h()
        return self.get_h() + self.g if h else None

    def change_parent(self, parent):
        self.parent = parent
        self.g = self.parent.g

    def non_barier(self):
        return self.value == settings.EMPTY_VALUE or self == self.ceil_point

    def was_passed(self, closed_point_list):
        return self in closed_point_list

    def __str__(self):
        return 'X: {x}, Y: {y}, g: {g}, h: {h}, f: {f}, value: {value}'.format(x=self.x, y=self.y, g=self.g,
                                                                               h=self.get_h(), f=self.get_f(),
                                                                               value=self.value)

    def __repr__(self):
        return '---X: {x}, Y: {y}, g: {g}, h: {h}, f: {f}---'.format(x=self.x, y=self.y, g=self.g, h=self.get_h(),
                                                                     f=self.get_f())

    def __gt__(self, other):
        return self.get_f() > other.get_f()

    def __ge__(self, other):
        return self.get_f() >= other.get_f()

    def __lt__(self, other):
        return self.get_f() < other.get_f()

    def __le__(self, other):
        return self.get_f() <= other.get_f()


def check_path(matrix, start_point, end_point):
    """
    Function for check path between start_point and end_point on matrix points. A* implementation
    :param matrix:
    :param start_point:
    :param end_point:
    :return True if path found and False in another:
    """
    open_point_list = []
    closed_point_list = []
    end_point = Point(end_point.x, end_point.y, end_point.value)
    start_point = Point(start_point.x, start_point.y, end_point.value, end_point)
    matrix = Matrix(copy.deepcopy(matrix))

    if end_point == start_point:
        return False

    closed_point_list.append(start_point)

    open_point_list.extend(matrix.get_around_points(start_point, closed_point_list))
    heapq.heapify(open_point_list)

    while open_point_list and end_point not in open_point_list:
        current_point = heapq.heappop(open_point_list)
        closed_point_list.append(current_point)

        around_points = matrix.get_around_points(current_point, closed_point_list)

        for point in around_points:
            if point not in open_point_list:
                point.change_parent(current_point)
                heapq.heappush(open_point_list, point)
            else:
                if point.g < current_point.g:
                    point.change_parent(current_point)

    return bool(open_point_list and end_point in open_point_list)
