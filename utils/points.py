import random
from itertools import chain

import base
import settings


def get_data_from_source_value(source_value):
    clear_func = lambda s: s.strip().strip('"').strip()
    value, div_postfix = map(clear_func, source_value.split(settings.SOURCE_VALUE_SPLITTER, 1))
    return value if value else None, div_postfix

def convert_source_to_matrix(source_list=None):
    """
    :return: matrix of BasePoint
    """
    source_list = source_list[:] if source_list else []
    matrix = [[base.BasePoint(w, h) for w in range(settings.PLAY_FIELD_WIDTH)]
              for h in range(settings.PLAY_FIELD_HEIGHT)]
    for point_data in source_list:
        point_value, x, y = point_data
        point_value, point_div_postfix = get_data_from_source_value(point_value)
        matrix[x][y].value = point_value
        matrix[x][y].div_postfix = point_div_postfix
    return matrix


def get_next_points(matrix, amount_next_points=settings.AMOUNT_NEXT_POINTS):
    empty_points = tuple(chain(*[filter(lambda point: point.value == settings.EMPTY_VALUE, line) for line in matrix]))
    try:
        selected_points = random.sample(empty_points, amount_next_points)
    except ValueError:
        selected_points = empty_points[:]
    point_values = random.sample(settings.VALUE_CHOICES, len(selected_points))
    return [setattr(point, 'value', point_values[point_index]) or point
            for point_index, point in enumerate(selected_points)]
