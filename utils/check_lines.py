import copy
import settings
from base import BasePoint


def _check_line(current_point, prev_point, values_line, is_last):
    values_line = values_line[:]
    disable_points = []

    if current_point.value != settings.EMPTY_VALUE and ((prev_point and current_point.value == prev_point.value) or not values_line):
        values_line.append(current_point)
    else:
        if len(values_line) >= settings.CRITICAL_LINE_LEN:
            disable_points = values_line[:]
        values_line = [current_point]

    if is_last and len(values_line) >= settings.CRITICAL_LINE_LEN:
        disable_points = values_line[:]

    return current_point, values_line, disable_points


def calc_scored_points(amount_points):
    result = 0
    if amount_points in settings.SCORED_POINTS:
        result = settings.SCORED_POINTS[amount_points]
    elif amount_points > 0:
        min_points = min(settings.SCORED_POINTS.keys())
        max_points = max(settings.SCORED_POINTS.keys())
        if amount_points < min_points:
            result = settings.SCORED_POINTS[min_points]
        elif amount_points > max_points:
            result = settings.SCORED_POINTS[max_points]
    return result


def check_lines(matrix):

    matrix = copy.deepcopy(matrix)

    func_disable_points = lambda points: [matrix[point.y].__setitem__(point.x, point.__class__(point.x, point.y,
                                                                                               settings.EMPTY_VALUE))
                                          for point in points[:]]
    burn_points = []
    scored_points = 0

    for i in range(len(matrix)):
        vert_line = []
        goriz_line = []
        vert_value = None
        goriz_value = None
        for j in range(0, len(matrix[0])):
            is_last = j == (len(matrix[0])-1)
            vert_value, vert_line, disable_points = _check_line(matrix[j][i], vert_value, vert_line, is_last)
            burn_points.extend(disable_points)
            func_disable_points(disable_points)
            scored_points += calc_scored_points(len(disable_points))
            goriz_value, goriz_line, disable_points = _check_line(matrix[i][j], goriz_value, goriz_line, is_last)
            burn_points.extend(disable_points)
            func_disable_points(disable_points)
            scored_points += calc_scored_points(len(disable_points))

    return burn_points, scored_points
