import os
import sqlite3
import settings


def create_tables():
    if os.path.isfile(settings.DB_PATH):
        os.remove(settings.DB_PATH)

    con = sqlite3.connect(settings.DB_NAME)
    with con:
        cur = con.cursor()
        cur.execute('CREATE TABLE client (id INTEGER PRIMARY KEY AUTOINCREMENT,'
                    'client_key VARCHAR(20) UNIQUE)')
        con.commit()

        cur.execute('CREATE TABLE client_history (id INTEGER PRIMARY KEY AUTOINCREMENT,'
                    'author_id INTEGER,'
                    'play_field VARCHAR(512),'
                    'start_point VARCHAR(20),'
                    'end_point VARCHAR(20),'
                    'scored_points INTEGER,'
                    'create_date DATETIME,'
                    'FOREIGN KEY(author_id) REFERENCES client(id))'
                    )

        con.commit()

        cur = con.cursor()
        cur.execute('SELECT SQLITE_VERSION()')

def get_connect():
    return sqlite3.connect(settings.DB_PATH)

def get_client_ids():
    con = get_connect()
    with con:
        cur = con.cursor()
        cur.execute('SELECT id FROM client')
        result = cur.fetchall()
    return result

def get_client_by_key(key):
    con = get_connect()
    with con:
        cur = con.cursor()
        sql = 'SELECT id FROM client WHERE client_key="{client_key}"'.format(client_key=key)
        cur.execute(sql)
        result = cur.fetchone()
    if result:
        result = result[0]
    return result


def get_client_history(client_id):
    con = get_connect()
    with con:
        cur = con.cursor()
        sql = 'SELECT id, author_id, play_field, start_point, end_point, scored_points ' \
              'FROM client_history WHERE author_id={client_id} ORDER BY datetime(create_date)'.\
            format(client_id=client_id)

        cur.execute(sql)
        result = cur.fetchall()
    return result

def get_history_item_by_id(history_id):
    con = get_connect()
    with con:
        cur = con.cursor()
        sql = 'SELECT id, author_id, play_field, start_point, end_point, scored_points ' \
              'FROM client_history WHERE id={history_id} ORDER BY datetime(create_date)'.\
            format(history_id=history_id)
        cur.execute(sql)
        result = cur.fetchone()
    return result


def get_history_before(history_id):
    con = get_connect()
    with con:
        cur = con.cursor()
        sql = 'SELECT id, author_id, play_field, start_point, end_point, scored_points ' \
              'FROM client_history WHERE id<={history_id} ORDER BY datetime(create_date)'.\
            format(history_id=history_id)
        cur.execute(sql)
        result = cur.fetchall()
    return result


def get_last_scored_points(client_id):
    con = get_connect()
    with con:
        cur = con.cursor()
        sql = 'SELECT scored_points FROM client_history WHERE author_id={client_id} ' \
              'ORDER BY datetime(create_date) DESC LIMIT 1'.format(client_id=client_id)
        cur.execute(sql)
        result = cur.fetchone()
        if result:
            result = result[0]
    return result


def write_new_client(key):
    con = get_connect()
    with con:
        cur = con.cursor()
        sql = 'INSERT INTO client (client_key) VALUES ("{key}")'.format(key=key)
        cur.execute(sql)
        con.commit()


def write_history_item(client_id, play_field, start_point, end_point, scored_points):
    con = get_connect()
    with con:
        cur = con.cursor()
        sql = """
              INSERT INTO client_history (author_id, play_field, start_point, end_point, scored_points, create_date)
              VALUES ({client_id}, '{play_field}', '{start_point}', '{end_point}', {scored_points}, DateTime('now'))""".\
              format(client_id=client_id, play_field=play_field, start_point=start_point, end_point=end_point,
                     scored_points=scored_points)
        cur.execute(sql)
        con.commit()
        return cur.lastrowid


def clean_history(client_id):
    con = get_connect()
    with con:
        cur = con.cursor()
        sql = 'DELETE FROM client_history WHERE author_id={client_id}'.format(client_id=client_id)
        cur.execute(sql)
        con.commit()

def clean_history_after(client_id, history_id):
    con = get_connect()
    with con:
        cur = con.cursor()
        sql = 'DELETE FROM client_history WHERE author_id={client_id} and id>{history_id}'.\
            format(client_id=client_id, history_id=history_id)
        cur.execute(sql)
        con.commit()
