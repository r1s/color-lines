import sys
import os
import unittest

sys.path.append(os.path.dirname(os.path.abspath(__file__)))

import settings

suite = unittest.TestSuite()
for all_test_suite in unittest.defaultTestLoader.discover(settings.TESTS_DIR, pattern=settings.TESTS_FILE_PATTERN):
    for test_suite in all_test_suite:
        suite.addTests(test_suite)

if __name__ == '__main__':
    unittest.TextTestRunner().run(suite)
