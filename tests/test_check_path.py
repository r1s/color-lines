import unittest
from unittest import TestCase

from base import BasePoint
import settings
from utils.check_path import check_path


open_matrix = [[1, 2, 1, 0, 2, 0, 3, 1, 3],
               [2, 0, 2, 0, 0, 3, -1, 2, 1],
               [3, 3, 3, 0, -1, -1, -1, -1, -1],
               [1, -1, -1, -1, -1, 1, 2, 1, -1],
               [-1, 1, -1, 2, 3, -1, 1, -1, -1],
               [1, -1, -1, 1, -1, 1, 2, 2, 0],
               [1, 1, 1, 3, -1, 2, 2, 2, 1],
               [2, 1, 1, 0, -1, 3, 0, 2, 3],
               [-1, 2, 1, 2, 3, 1, 3, 2, -1]]

close_matrix = [[1, 2, 1, 0, 2, 0, 3, 1, 3],
               [2, 0, 2, 0, 0, 3, -1, 2, 1],
               [3, 3, 3, 0, -1, -1, -1, -1, -1],
               [1, -1, -1, 1, -1, 1, 2, 1, -1],
               [-1, 1, -1, 2, 3, -1, -1, -1, 1],
               [1, -1, -1, 1, -1, 1, 2, 2, 0],
               [1, 1, 1, 3, -1, 2, 2, 2, 1],
               [2, 1, 1, 0, -1, 3, 0, 2, 3],
               [-1, 2, 1, 2, 3, 1, 3, 2, -1]]


class TestCheckPath(TestCase):

    def get_point_matrix(self, matrix):
        return [[BasePoint(index_point, index_line, value if value >= 0 else settings.EMPTY_VALUE)
                 for index_point, value in enumerate(line)]
                for index_line, line in enumerate(matrix)]

    def get_closed_diag_matrix(self):
        matrix = [[BasePoint(index_point, index_line, 5 if index_point == index_line else settings.EMPTY_VALUE)
                   for index_point in range(settings.PLAY_FIELD_WIDTH)]
                  for index_line in range(settings.PLAY_FIELD_HEIGHT)]
        matrix[self.start_point.y][self.start_point.x] = self.start_point
        matrix[self.end_point.y][self.end_point.x] = self.end_point
        return matrix

    def setUp(self):
        super(TestCheckPath, self).setUp()
        self.open_matrix = self.get_point_matrix(open_matrix)
        self.close_matrix = self.get_point_matrix(close_matrix)
        self.start_point = BasePoint(1, 4, 1)
        self.end_point = BasePoint(8, 4, 1)

    def test_open_path(self):
        self.assertTrue(check_path(self.open_matrix, self.start_point, self.end_point))

        matrix = self.get_closed_diag_matrix()
        matrix[0][0] = BasePoint(0, 0, settings.EMPTY_VALUE)
        self.assertTrue(check_path(matrix, self.start_point, self.end_point))

    def test_close_matrix(self):
        self.assertFalse(check_path(self.close_matrix, self.start_point, self.end_point))

    def test_close_matrix_diag(self):
        matrix = self.get_closed_diag_matrix()
        self.assertFalse(check_path(matrix, self.start_point, self.end_point))


if __name__ == "__main__":
    unittest.main()
