from unittest import TestCase
from base import BasePoint
import settings
from utils.points import get_next_points


class TestCheckNextPoints(TestCase):

    def get_simple_matrix(self):
        return [[BasePoint(index_point, index_line, settings.EMPTY_VALUE if not index_point else index_point)
                 for index_point in range(settings.PLAY_FIELD_WIDTH)]
                for index_line in range(settings.PLAY_FIELD_HEIGHT)]

    def get_full_matrix(self):
        return [[BasePoint(index_point, index_line, int(str(index_line) + str(index_point)))
                 for index_point in range(settings.PLAY_FIELD_WIDTH)]
                for index_line in range(settings.PLAY_FIELD_HEIGHT)]

    def test_simple(self):
        matrix = self.get_simple_matrix()
        points = get_next_points(matrix)
        self.assertEqual(settings.AMOUNT_NEXT_POINTS, len(points))
        for point in points:
            self.assertNotEqual(point.value, settings.EMPTY_VALUE)

    def test_full(self):
        matrix = self.get_full_matrix()
        points = get_next_points(matrix)
        self.assertFalse(bool(points))

    def test_residue(self):
        matrix = self.get_full_matrix()
        matrix[0][0].value = settings.EMPTY_VALUE
        matrix[-1][-1].value = settings.EMPTY_VALUE
        points = get_next_points(matrix)
        self.assertEqual(2, len(points))
        for point in points:
            self.assertNotEqual(point.value, settings.EMPTY_VALUE)