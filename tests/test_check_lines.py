import unittest
from unittest import TestCase

from base import BasePoint
import settings
from utils.check_lines import check_lines


simple_burn_matrix = [[1, 2, 1, 0, 2, 0, 3, 1, 3],
                      [2, 0, 2, 0, 0, 3, -1, 2, 1],
                      [3, 3, 3, 0, -1, -1, -1, -1, -1],
                      [1, -1, -1, 0, -1, 1, 2, 1, -1],
                      [-1, 1, -1, 0, 3, -1, 1, -1, -1],
                      [1, -1, -1, 1, -1, 1, 2, 2, 0],
                      [1, 1, 1, 3, -1, 2, 2, 2, 1],
                      [2, 1, 1, 0, -1, 3, 0, 2, 3],
                      [-1, 2, 1, 2, 3, 1, 3, 2, -1]]

cross_burn_matrix = [[1, 2, 1, 0, 2, 0, 3, 1, 3],
                     [2, 0, 2, 0, 0, 3, -1, 2, 1],
                     [3, 3, 3, 0, -1, -1, -1, -1, -1],
                     [1, -1, -1, -1, 2, 1, 2, 1, -1],
                     [-1, 1, -1, 2, 2, -1, 1, -1, -1],
                     [1, -1, -1, 1, 2, 1, 2, 2, 0],
                     [1, 1, 1, 2, 2, 2, 2, 2, 1],
                     [2, 1, 1, 0, -1, 2, 0, 2, 3],
                     [-1, 2, 1, 2, 3, 1, 3, 2, -1]]

multi_burn_matrix = [[1, 2, 1, 0, 2, 0, 3, 1, 3],
                     [2, 0, 0, 0, 0, 0, -1, 2, 1],
                     [3, 3, 4, 0, -1, -1, -1, -1, -1],
                     [3, -1, 4, 0, -1, 1, 2, 1, -1],
                     [3, 1, 4, 0, 3, -1, 1, -1, -1],
                     [3, -1, 4, 1, -1, 1, 2, 2, 0],
                     [3, 1, 4, 3, 2, 2, 2, 2, 2],
                     [2, 1, 4, 0, -1, 3, 0, 2, 3],
                     [-1, 2, 4, 2, 3, 1, 3, 2, -1]]


class TestCheckLines(TestCase):

    def get_point_matrix(self, matrix):
        return [[BasePoint(index_point, index_line, value if value >= 0 else settings.EMPTY_VALUE)
                 for index_point, value in enumerate(line)]
                for index_line, line in enumerate(matrix)]

    def get_not_burn_matrix(self):
        return [[BasePoint(index_point, index_line, int(str(index_line) + str(index_point)))
                 for index_point in range(settings.PLAY_FIELD_WIDTH)]
                for index_line in range(settings.PLAY_FIELD_HEIGHT)]

    def setUp(self):
        super(TestCheckLines, self).setUp()
        self.simple_burn_matrix = self.get_point_matrix(simple_burn_matrix)
        self.cross_burn_matrix = self.get_point_matrix(cross_burn_matrix)
        self.multi_burn_matrix = self.get_point_matrix(multi_burn_matrix)

    def test_simple_burn(self):
        burn_points, scored_points = check_lines(self.simple_burn_matrix)
        self.assertEqual(5, len(burn_points))
        for burn_point in burn_points:
            self.assertEqual(0, burn_point.value)

    def test_cross_burn_matrix(self):
        burn_points, scored_points = check_lines(self.cross_burn_matrix)
        self.assertEqual(5, len(burn_points))
        for burn_point in burn_points:
            self.assertEqual(2, burn_point.value)

    def test_multi_burn_matrix(self):
        burn_points, scored_points = check_lines(self.multi_burn_matrix)
        self.assertEqual(22, len(burn_points))

    def test_not_burn_matrix(self):
        matrix = self.get_not_burn_matrix()
        burn_points, scored_points = check_lines(matrix)
        self.assertEqual(0, len(burn_points))


if __name__ == "__main__":
    unittest.main()

