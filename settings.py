import os

COOKIE_SECRET = "61oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo="
CLIENT_ID_COOKIE_NAME = 'client_id'
HOST = 'localhost'
PORT = 8888
AMOUNT_NEXT_POINTS = 3

PLAY_FIELD_WIDTH = 9
PLAY_FIELD_HEIGHT = 9

EMPTY_VALUE = None
VALUE_CHOICES = ['red', 'green', 'blue', 'black', 'yellow', 'brown', 'purple']

APP_PATH = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_PATH = os.path.join(APP_PATH, '')

TESTS_DIR = 'tests'
TESTS_FILE_PATTERN = 'test_*.py'

DB_NAME = '.lines.db'
DB_PATH = os.path.join(APP_PATH, DB_NAME)


CRITICAL_LINE_LEN = 5


SOURCE_VALUE_SPLITTER = '_'

SCORED_POINTS = {5: 10, 6: 12, 7: 18, 8: 28, 9: 42}
